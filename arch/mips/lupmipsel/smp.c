#include <linux/cpumask.h>
#include <linux/irqflags.h>
#include <linux/of.h>
#include <linux/sched.h>
#include <linux/sched/task_stack.h>

#include <asm/processor.h>
#include <asm/ptrace.h>
#include <asm/smp.h>

#include <asm/mach-lupmipsel/lupmipsel.h>

volatile unsigned long lupmipsel_smp_boot;
volatile unsigned long lupmipsel_smp_sp;
volatile unsigned long lupmipsel_smp_gp;

static void lupmipsel_smp_setup(void)
{
	struct device_node *dn;
	int cpuid = 1;

	/* We assume that the boot CPU is CPU0 */
	set_cpu_possible(0, true);
	__cpu_number_map[0] = 0;
	__cpu_logical_map[0] = 0;

	/* Detect other CPUs by scanning the FDT */
	for_each_of_cpu_node(dn) {
		int hwid = of_get_cpu_hwid(dn, 0);

		if (hwid >= NR_CPUS) {
			pr_warn("Invalid cpu with hwid %d\n", hwid);
			/* Ignore CPU */
			continue;
		}

		if (hwid == 0)
			/* Ignore boot CPU */
			continue;

		set_cpu_possible(cpuid, true);
		set_cpu_present(cpuid, true);

		__cpu_number_map[hwid] = cpuid;
		__cpu_logical_map[cpuid] = hwid;

		cpuid++;
	}

	pr_info("Detected %i available CPU(s)\n", cpuid);

	/* From smp-cps and ip30, don't know if it's necessary or not */
	change_c0_config(CONF_CM_CMASK, CONF_CM_CACHABLE_COW);
}

static void lupmipsel_smp_prepare_cpus(unsigned int max_cpus)
{
	/* Nothing to do */
	return;
}

static int lupmipsel_smp_boot_secondary(int cpu, struct task_struct *idle)
{
	int hwid = cpu_logical_map(cpu);

	pr_info("SMP: booting cpu %d (hwid %d)...\n", cpu, hwid);

	lupmipsel_smp_sp = __KSTK_TOS(idle);
	lupmipsel_smp_gp = (unsigned long)(task_thread_info(idle));
	lupmipsel_smp_boot = hwid;
	/* Flush writes -- Secondary CPUs are spinning on lupmipsel_smp_boot's
	 * value until their hwid appears */
	mb();

	return 0;
}

static void lupmipsel_smp_init_secondary(void)
{
	/* Enable all hardware IRQs at CPU level */
	change_c0_status(ST0_IM, STATUSF_IP2 | STATUSF_IP3 |
			 STATUSF_IP4 | STATUSF_IP5 |
			 STATUSF_IP6 | STATUSF_IP7);
}

static void lupmipsel_smp_finish(void)
{
	local_irq_enable();
}

struct plat_smp_ops lupmipsel_smp_ops = {
	/* Generic callbacks */
	.send_ipi_single	= mips_smp_send_ipi_single,
	.send_ipi_mask		= mips_smp_send_ipi_mask,

	/* Custom callbacks (in order of being called) */

	/* Called by boot CPU during start_kernel() */
	.smp_setup		= lupmipsel_smp_setup,
	//.prepare_boot_cpu	= lupmipsel_smp_init_cpu,

	/* Called by boot CPU in init kthread */
	.prepare_cpus		= lupmipsel_smp_prepare_cpus,
	.boot_secondary		= lupmipsel_smp_boot_secondary,

	/* Called by each secondary CPU */
	.init_secondary		= lupmipsel_smp_init_secondary,
	.smp_finish		= lupmipsel_smp_finish,
};
