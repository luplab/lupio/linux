#ifndef __ASM_MACH_LUPMIPSEL_KERNEL_ENTRY_INIT_H
#define __ASM_MACH_LUPMIPSEL_KERNEL_ENTRY_INIT_H

/*
 * First macro called by all CPUs in head.S:kernel_entry()
 */
.macro	kernel_entry_setup
	.set push
	.set noreorder
	# Get CPU hwid
	rdhwr	a2, $0
	# Only proceed back to kernel_entry if boot CPU
	beq	a2, zero, lupmipsel_boot_cpu
	nop

	# Now secondary CPUs wait to be booted
lupmipsel_spin_boot:
	# Wait until our number gets called
	PTR_LA	t0, lupmipsel_smp_boot
	LONG_L	t1, (t0)
	bne	t1, a2, lupmipsel_spin_boot
	nop

	# Yay, our number was called, get our gp and sp
	PTR_LA	t0, lupmipsel_smp_gp
	LONG_L	gp, (t0)
	PTR_LA	t0, lupmipsel_smp_sp
	LONG_L	sp, (t0)

	# Set sp back to 0 to let boot CPU that we're good
	LONG_S	zero, (t0)
	sync

	# Jump to entry point for secondary CPUs
	j smp_bootstrap
	nop

lupmipsel_boot_cpu:
	.set pop
.endm

/*
 * First macro called by secondary CPUs in head.S:smp_bootstrap()
 */
.macro	smp_slave_setup
	/* Nothing necessary here */
.endm

#endif /* __ASM_MACH_LUPMIPSEL_KERNEL_ENTRY_INIT_H */
