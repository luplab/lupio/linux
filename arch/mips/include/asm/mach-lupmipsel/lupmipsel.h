#ifndef __ASM_MACH_LUPMIPSEL_LUPMIPSEL_H
#define __ASM_MACH_LUPMIPSEL_LUPMIPSEL_H

#ifdef CONFIG_SMP
extern struct plat_smp_ops lupmipsel_smp_ops;
#endif

#endif /* __ASM_MACH_LUPMIPSEL_LUPMIPSEL_H */
