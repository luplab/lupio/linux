// SPDX-License-Identifier: GPL-2.0

#include <linux/mm.h>
#include <linux/smp.h>
#include <linux/sched.h>
#include <asm/sbi.h>
#include <asm/mmu_context.h>

static inline void local_flush_tlb_all_asid(unsigned long asid)
{
	__asm__ __volatile__ ("sfence.vma x0, %0"
			:
			: "r" (asid)
			: "memory");
}

static inline void local_flush_tlb_page_asid(unsigned long addr,
		unsigned long asid)
{
	__asm__ __volatile__ ("sfence.vma %0, %1"
			:
			: "r" (addr), "r" (asid)
			: "memory");
}

#define TLB_FLUSH_ALL -1UL

struct flush_tlb_info {
       unsigned long start;
       unsigned long size;
       unsigned long stride;
       unsigned long asid;
};

static DEFINE_PER_CPU_SHARED_ALIGNED(struct flush_tlb_info, flush_tlb_info);

static void __tlb_flush_local(const struct flush_tlb_info *f)
{
	if (f->size <= f->stride)
		local_flush_tlb_page(f->start);
	else
		local_flush_tlb_all();
}

static void __tlb_flush_local_asid(const struct flush_tlb_info *f)
{
	if (f->size <= f->stride)
		local_flush_tlb_page_asid(f->start, f->asid);
	else
		local_flush_tlb_all_asid(f->asid);
}

static void ipi_remote_sfence_vma(void *info)
{
       const struct flush_tlb_info *f = info;

       __tlb_flush_local(f);
}

static void ipi_remote_sfence_vma_asid(void *info)
{
       const struct flush_tlb_info *f = info;

       __tlb_flush_local_asid(f);
}

static void __tlb_flush_remote(const struct cpumask *cmask,
			       struct flush_tlb_info *f)
{
	if (static_branch_unlikely(&use_asid_allocator)) {
		if (IS_ENABLED(CONFIG_RISCV_SBI))
			sbi_remote_sfence_vma_asid(cmask, f->start, f->size, f->asid);
		else
			on_each_cpu_mask(cmask, ipi_remote_sfence_vma_asid, f, 1);
	} else {
		if (IS_ENABLED(CONFIG_RISCV_SBI))
			sbi_remote_sfence_vma(cmask, f->start, f->size);
		else
			on_each_cpu_mask(cmask, ipi_remote_sfence_vma, f, 1);
	}
}

static void __tlb_flush_range(struct mm_struct *mm, unsigned long start,
				  unsigned long size, unsigned long stride)
{
	struct cpumask *cmask = mm_cpumask(mm);
	unsigned int cpuid;
	bool broadcast;
	struct flush_tlb_info *info;

	if (cpumask_empty(cmask))
		return;

	cpuid = get_cpu();

	info = this_cpu_ptr(&flush_tlb_info);
	info->start = start;
	info->size = size;
	info->stride = stride;
	info->asid = atomic_long_read(&mm->context.id);

	/* check if the tlbflush needs to be sent to other CPUs */
	broadcast = cpumask_any_but(cmask, cpuid) < nr_cpu_ids;
	if (broadcast) {
		__tlb_flush_remote(cmask, info);
	} else {
		if (static_branch_unlikely(&use_asid_allocator))
			__tlb_flush_local_asid(info);
		else
			__tlb_flush_local(info);
	}

	put_cpu();
}

void flush_tlb_mm(struct mm_struct *mm)
{
	__tlb_flush_range(mm, 0, TLB_FLUSH_ALL, PAGE_SIZE);
}

void flush_tlb_page(struct vm_area_struct *vma, unsigned long addr)
{
	__tlb_flush_range(vma->vm_mm, addr, PAGE_SIZE, PAGE_SIZE);
}

void flush_tlb_range(struct vm_area_struct *vma, unsigned long start,
		     unsigned long end)
{
	__tlb_flush_range(vma->vm_mm, start, end - start, PAGE_SIZE);
}
#ifdef CONFIG_TRANSPARENT_HUGEPAGE
void flush_pmd_tlb_range(struct vm_area_struct *vma, unsigned long start,
			unsigned long end)
{
	__tlb_flush_range(vma->vm_mm, start, end - start, PMD_SIZE);
}
#endif

void flush_tlb_all(void)
{
	struct flush_tlb_info *info;

	get_cpu();

	info = this_cpu_ptr(&flush_tlb_info);
	info->start = 0;
	info->size = TLB_FLUSH_ALL;
	info->stride = PAGE_SIZE;

	on_each_cpu(ipi_remote_sfence_vma, info, 1);

	put_cpu();
}
