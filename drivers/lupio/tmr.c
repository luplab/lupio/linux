// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2021 Joël Porquet-Lupine <joel@porquet.org>
 */

#include <linux/clk.h>
#include <linux/clockchips.h>
#include <linux/clocksource.h>
#include <linux/cpuhotplug.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <linux/kernel.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>

#include <asm/io.h>
#ifdef CONFIG_ARM
#include <asm/smp_plat.h>
#else
#include <asm/smp.h>
#endif
#include <asm/timex.h>

/*
 * Driver for LupIO-TMR
 */
#define LUPIO_TMR "lupio-tmr"

/*
 * The clockevent API doesn't provide any mechanism to store private data, and
 * it's a massive pain to work around it if one wants to still allocate private
 * data dynamically. So let's use a few global variables and simplify the design
 * of this driver.
 */
static void __iomem *virt_base;
static unsigned int tmr_irq;
static u32 freq;
static DEFINE_PER_CPU(struct clock_event_device, *events);

/* LupIO-TMR register map */
#define LUPIO_TMR_TIME	0x0	// R: Monotonic counter
#define LUPIO_TMR_LOAD	0x4	// RW: Reload value
#define LUPIO_TMR_CTRL	0x8	// W: Timer control
#define LUPIO_TMR_STAT	0xC	// R: Timer status

#define LUPIO_TMR_MAX	0x10	// Stride for SMP

#define LUPIO_TMR_REG(idx, reg)	((idx) * LUPIO_TMR_MAX + reg)

/* CTRL/STAT fields */
#define LUPIO_TMR_IRQE	0x1	// CTRL
#define LUPIO_TMR_PRDC	0x2	// CTRL

#define LUPIO_TMR_EXPD	0x1	// STAT

/*
 * Interrupt handler
 */
static irqreturn_t lupio_tmr_interrupt(int irq, void *dev_id)
{
	struct clock_event_device *evt = dev_id;
	unsigned int cpuid = cpu_logical_map(smp_processor_id());
	unsigned long stat;

	/* Acknowledge interrupt from expired timer */
	stat = readl(virt_base + LUPIO_TMR_REG(cpuid, LUPIO_TMR_STAT));
	if (!(stat & LUPIO_TMR_EXPD))
		return IRQ_NONE;

	/* Call event handler */
	evt->event_handler(evt);

	return IRQ_HANDLED;
}

/*
 * Clockevent driver
 */
static void lupio_tmr_setup(u32 reload, bool periodic)
{
	unsigned int cpuid = cpu_logical_map(smp_processor_id());

	/* Timer value */
	writel(reload, virt_base + LUPIO_TMR_REG(cpuid, LUPIO_TMR_LOAD));

	/* Start timer (or stop ongoing timer if reload value is 0) */
	writel(LUPIO_TMR_IRQE + (periodic ? LUPIO_TMR_PRDC : 0),
	       virt_base + LUPIO_TMR_REG(cpuid, LUPIO_TMR_CTRL));
}

static int lupio_tmr_shutdown(struct clock_event_device *evt)
{
	lupio_tmr_setup(0, false);

	return 0;
}

static int lupio_tmr_set_periodic(struct clock_event_device *evt)
{
	lupio_tmr_setup(DIV_ROUND_CLOSEST(freq, HZ), true);

	return 0;
}

static int lupio_tmr_set_next_event(unsigned long delta,
				    struct clock_event_device *evt)
{
	lupio_tmr_setup(delta, false);

	return 0;
}

/*
 * CPU hotplug
 */
static int __init lupio_tmr_clockevent_init(unsigned int cpu)
{
	struct clock_event_device *evt = per_cpu_ptr(events, cpu);

	/* Configure clockevent driver */
	evt->features = CLOCK_EVT_FEAT_PERIODIC | CLOCK_EVT_STATE_ONESHOT;
	evt->set_next_event = lupio_tmr_set_next_event;
	evt->set_state_periodic = lupio_tmr_set_periodic;
	evt->set_state_shutdown = lupio_tmr_shutdown;
	evt->rating = 300;
	evt->cpumask = cpumask_of(cpu);

	/* Enable IRQ now that the CPU is up */
	enable_percpu_irq(tmr_irq, IRQ_TYPE_NONE);

	/* Register clockevent driver */
	clockevents_config_and_register(evt, freq, 1, U32_MAX);

	return 0;
}

static int __init lupio_tmr_clockevent_deinit(unsigned int cpu)
{
	struct clock_event_device *evt = per_cpu_ptr(events, cpu);

	evt->set_state_shutdown(evt);
	disable_percpu_irq(tmr_irq);

	return 0;
}

/*
 * OF/Device tree driver
 */
static int __init lupio_tmr_init(struct device_node *of_node)
{
	struct clk *clk;
	unsigned int cpuid = cpu_logical_map(smp_processor_id());
	int err = 0;

	/* Allocate private data */
	events = alloc_percpu(struct clock_event_device);
	if (!events) {
		err = -ENOMEM;
		goto error;
	}

	/* Map timer in memory */
	virt_base = of_io_request_and_map(of_node, 0, of_node->name);
	if (IS_ERR(virt_base)) {
		err = PTR_ERR(virt_base);
		goto error_alloc;
	}

	/* Get timer's clock */
	clk = of_clk_get(of_node, 0);
	if (!IS_ERR(clk)) {
		err = clk_prepare_enable(clk);
		if (err) {
			pr_err(LUPIO_TMR ": Failed to prepare clk\n");
			clk_put(clk);
			goto error_map;
		}
	} else {
		pr_err(LUPIO_TMR ": Failed to find clk\n");
		err = PTR_ERR(clk);
		goto error_map;
	}

	/* Get clock's frequency */
	freq = clk_get_rate(clk);
	if (!freq) {
		pr_err(LUPIO_TMR ": Failed to get clock rate\n");
		err = -EINVAL;
		goto error_clk;
	}

	/* Initialize clocksource (monotonic counter for time keeping) */
	err = clocksource_mmio_init(virt_base + LUPIO_TMR_TIME,
				     of_node->name, freq, 300, 32,
				     clocksource_mmio_readl_up);
	if (err)
		goto error_clk;

#ifdef CONFIG_RISCV
	riscv_time_val = virt_base + LUPIO_TMR_TIME;
#endif

	/* Configure IRQ for all the timers */
	tmr_irq = irq_of_parse_and_map(of_node, cpuid);
	if (!tmr_irq) {
		pr_err(LUPIO_TMR ": Failed to map IRQ\n");
		err = -EINVAL;
		goto error_clk;
	}

	/* Associate IRQ handler but don't enable IRQ just yet */
	irq_set_status_flags(tmr_irq, IRQ_NOAUTOEN);
	err = request_percpu_irq(tmr_irq, lupio_tmr_interrupt,
				 of_node->name, events);
	if (err) {
		pr_err(LUPIO_TMR ": Failed to request IRQ\n");
		err = -EINVAL;
		goto error_irq;
	}

	/* Install hotplug callbacks to configure clockevent
	 * (periodic/oneshot timer) */
	err = cpuhp_setup_state(CPUHP_AP_LUPIO_TMR_STARTING,
				"clockevents/lupio/tmr:starting",
				lupio_tmr_clockevent_init,
				lupio_tmr_clockevent_deinit);
	if (err) {
		pr_err(LUPIO_TMR ": Failed to install hotplug callbacks\n");
		goto error_irq_req;
	}

	return 0;

error_irq_req:
	free_irq(tmr_irq, events);
error_irq:
	irq_dispose_mapping(tmr_irq);
error_clk:
	clk_disable_unprepare(clk);
	clk_put(clk);
error_map:
	iounmap(virt_base);
error_alloc:
	free_percpu(events);
error:
	pr_err(LUPIO_TMR ": Failed to initialize node %s (err=%d)\n",
	       of_node->name, err);
	return err;
}

TIMER_OF_DECLARE(lupio_tmr, "lupio,tmr", lupio_tmr_init);
