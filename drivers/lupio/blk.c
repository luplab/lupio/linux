// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2021 Joël Porquet-Lupine <joel@porquet.org>
 */
#include <linux/blk-mq.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/slab.h>

#include <asm/io.h>


/*
 * Driver for LupIO-blk
 */

#define DEVICE_NAME	"lupio_blk"

/* Max number of partitions per device */
#define BLK_MINORS	16


struct lupio_blk_data {
	spinlock_t lock;
	struct device *dev;
	void __iomem *virt_base;
	unsigned int irq;
	unsigned int index;
	struct blk_mq_tag_set tag_set;
	struct gendisk *gd;
	struct request *req;
};

struct lupio_blk_driver {
	int major;
	unsigned int num;
	struct lupio_blk_data **blks;
};

static struct lupio_blk_driver *lupio_blk_driver;

/* LupIO-BLK register map */
#define LUPIO_BLK_CONF	0x0	// R: device configuration
#define LUPIO_BLK_NBLK	0x4	// R/W: num blocks to transfer
#define LUPIO_BLK_BLKA	0x8	// R/W: block address
#define LUPIO_BLK_MEMA	0xC	// R/W: memory address
#define LUPIO_BLK_CTRL	0x10	// W: Transfer control
#define LUPIO_BLK_STAT	0x14	// R: Transfer status

/* CTRL/STAT fields */
#define LUPIO_BLK_IRQE	0x1	// CTRL
#define LUPIO_BLK_BUSY	0x1	// STAT
#define LUPIO_BLK_TYPE	0x2	// CTRL+STAT
#define LUPIO_BLK_ERRR	0x4	// STAT


/*
 * Utils
 */

static blk_status_t lupio_blk_submit_request(struct lupio_blk_data *blk,
				     struct request *req)
{
	unsigned int nblk;
	sector_t lba;
	unsigned long mem, ctrl;

	/* Get request parameters */
	nblk = blk_rq_sectors(req);
	lba = blk_rq_pos(req);
	mem = virt_to_phys(bio_data(req->bio));
	ctrl = LUPIO_BLK_IRQE | (rq_data_dir(req) ? LUPIO_BLK_TYPE : 0);

	dev_dbg(blk->dev, "ctrl=%lu, lba=%llu, nblk=%u vmem=0x%px pmem=0x%lx",
		ctrl, lba, nblk, bio_data(req->bio), mem);

	/* Configure transfer on device */
	writel(nblk, blk->virt_base + LUPIO_BLK_NBLK);
	writel(lba, blk->virt_base + LUPIO_BLK_BLKA);
	writel(mem, blk->virt_base + LUPIO_BLK_MEMA);

	/* Launch transfer */
	writel(ctrl, blk->virt_base + LUPIO_BLK_CTRL);

	/* Keep track of request to access it in IRQ handler */
	blk->req = req;

	return BLK_STS_OK;
}

static blk_status_t lupio_blk_do_request(struct lupio_blk_data *blk,
					 struct request *req)
{
	switch(req_op(req)) {
		case REQ_OP_READ:
		case REQ_OP_WRITE:
			return lupio_blk_submit_request(blk, req);
		default:
			blk_dump_rq_flags(req, DEVICE_NAME " bad request");
			return BLK_STS_IOERR;
	}
}

static blk_status_t lupio_blk_queue_rq(struct blk_mq_hw_ctx *hctx,
				       const struct blk_mq_queue_data *bd)
{
	struct request_queue *q = hctx->queue;
	struct lupio_blk_data *blk = q->queuedata;
	blk_status_t ret;

	blk_mq_start_request(bd->rq);

	spin_lock_irq(&blk->lock);
	ret = lupio_blk_do_request(blk, bd->rq);
	spin_unlock_irq(&blk->lock);

	return ret;
}

static irqreturn_t lupio_blk_interrupt(int irq, void *dev_id)
{
	struct lupio_blk_data *blk = dev_id;
	struct request *req = blk->req;
	unsigned long status;
	blk_status_t error;

	/* Reading STATUS register resets IRQ */
	status = readl(blk->virt_base + LUPIO_BLK_STAT);

	/* Examine status */
	if (status & LUPIO_BLK_ERRR) {
		dev_err(blk->dev, "%s: failure\n", __func__);
		error = BLK_STS_IOERR;
	} else {
		dev_dbg(blk->dev, "%s: success\n", __func__);
		error = 0;
	}

	/* Make request as completed */
	spin_lock(&blk->lock);
	blk->req = NULL;
	blk_mq_end_request(req, error);
	spin_unlock(&blk->lock);

	/* Check for other requests */
	blk_mq_run_hw_queues(blk->gd->queue, true);
	return IRQ_HANDLED;
}

/*
 * Block device operations
 */

static const struct block_device_operations lupio_blk_fops = {
	.owner		= THIS_MODULE,
};

static const struct blk_mq_ops lupio_blk_mqops = {
	.queue_rq = lupio_blk_queue_rq,
};

/*
 * Platform driver
 */

static int lupio_blk_pf_probe(struct platform_device *pdev)
{
	struct lupio_blk_data **blks;
	struct lupio_blk_data *blk;
	struct request_queue *q;
	unsigned int index;
	int err;

	unsigned long conf, sect_sz, sect_num;

	/* Get our private structure */
	blks = lupio_blk_driver->blks;

	/* Look for a free index if any */
	for (index = 0; index < lupio_blk_driver->num; index++)
		if (blks[index] == NULL)
			break;
	if (index >= lupio_blk_driver->num)
		return -EINVAL;

	/* Allocate a new lupio_blk_data instance */
	blk = devm_kzalloc(&pdev->dev, sizeof(struct lupio_blk_data),
			   GFP_KERNEL);
	if (!blk) {
		dev_err(&pdev->dev, "failed to allocate memory for %s node\n",
				pdev->name);
		return -ENOMEM;
	}
	blks[index] = blk;
	blk->index = index;

	/* Get the virq of the irq that links the lupio_blk to the parent
	 * interrupt controller.  Note that the virq has already been computed
	 * beforehand, with respect to the irq_domain it belongs to. */
	blk->irq = platform_get_irq(pdev, 0);
	if (blk->irq < 0) {
		dev_err(&pdev->dev, "failed to get virq for %s node\n",
				pdev->name);
		err = -EINVAL;
		goto error_blk;
	}

	/* Map device in memory */
	blk->virt_base = devm_platform_ioremap_resource(pdev, 0);
	if (!blk->virt_base) {
		dev_err(&pdev->dev, "failed to ioremap_resource for %s node\n",
				pdev->name);
		err = -EADDRNOTAVAIL;
		goto error_blk;
	}

	/* Get block device configuration */
	conf = readl(blk->virt_base + LUPIO_BLK_CONF);
	sect_sz = 1UL << (conf >> 16);
	sect_num = 1UL << (conf & 0xFFFF);

	/* Support 512-byte sectors only */
	if (sect_sz != 512) {
		dev_err(&pdev->dev, "%lu-byte sectors but only 512-byte sectors supported\n",
			sect_sz);
		err = -EINVAL;
		goto error_blk;
	}
	dev_info(&pdev->dev, "%lu %lu-byte sectors\n", sect_num, sect_sz);

	/* Complete the initialization of our private data structure */
	blk->dev = &pdev->dev;
	spin_lock_init(&blk->lock);

	/* Allocate queue */
	err = blk_mq_alloc_sq_tag_set(&blk->tag_set, &lupio_blk_mqops, 1,
				      BLK_MQ_F_SHOULD_MERGE);
	if (err) {
		dev_err(&pdev->dev, "failed to allocate blk queue\n");
		goto error_blk;
	}

	/* Allocate disk */
	blk->gd = blk_mq_alloc_disk(&blk->tag_set, blk);
	if (IS_ERR(blk->gd)) {
		err = PTR_ERR(blk->gd);
		goto error_free_tags;
	}

	/* Configure queue */
	q = blk->gd->queue;
	/* Force 1 segment per BIO request as we can only do one DMA mapping */
	blk_queue_max_segments(q, 1);
	/* DMA mapping only on 32-bits */
	blk_queue_bounce_limit(q, U32_MAX);

	/* Configure generic hard drive layer */
	blk->gd->major = lupio_blk_driver->major;
	blk->gd->first_minor = index * BLK_MINORS;
	blk->gd->minors = BLK_MINORS;
	blk->gd->fops = &lupio_blk_fops;
	blk->gd->private_data = blk;
	snprintf(blk->gd->disk_name, sizeof(blk->gd->disk_name),
		 "ld%c", index + 'a');
	/* Configure disk capacity */
	set_capacity(blk->gd, sect_num);

	/* Setup irq */
	err = devm_request_irq(&pdev->dev, blk->irq, lupio_blk_interrupt, 0,
			       DEVICE_NAME, blk);
	if (err) {
		dev_err(&pdev->dev, "could not request irq %u for %s node\n",
			blk->irq, pdev->name);
		goto error_cleanup_disk;
	}

	err = add_disk(blk->gd);
	if (err) {
		dev_err(&pdev->dev, "failed to add disk\n");
		goto error_cleanup_disk;
	}

	platform_set_drvdata(pdev, blk);

	return 0;

error_cleanup_disk:
	blk_cleanup_disk(blk->gd);
error_free_tags:
	blk_mq_free_tag_set(&blk->tag_set);
error_blk:
	blks[blk->index] = NULL;
	return err;
}

static int lupio_blk_pf_remove(struct platform_device *pdev)
{
	struct lupio_blk_data **blks = lupio_blk_driver->blks;
	struct lupio_blk_data *blk = platform_get_drvdata(pdev);

	del_gendisk(blk->gd);
	blk_cleanup_disk(blk->gd);
	blk_mq_free_tag_set(&blk->tag_set);

	blks[blk->index] = NULL;
	platform_set_drvdata(pdev, NULL);

	return 0;
}

#define LUPIO_BLK_OF_COMPATIBLE "lupio,blk"

static const struct of_device_id lupio_blk_pf_of_ids[] = {
	{ .compatible = LUPIO_BLK_OF_COMPATIBLE },
	{}
};
MODULE_DEVICE_TABLE(of, lupio_blk_pf_of_ids);

static struct platform_driver lupio_blk_pf_driver = {
	.driver = {
		.owner		= THIS_MODULE,
		.name		= DEVICE_NAME,
		.of_match_table = lupio_blk_pf_of_ids,
	},
	.probe	= lupio_blk_pf_probe,
	.remove	= lupio_blk_pf_remove,
};

/*
 * Module initialization and termination
 */

static int __init lupio_blk_init(void)
{
	struct device_node *np;
	unsigned int count = 0;
	int err;

	pr_debug("%s: registering LupIO-blk driver\n", __func__);

	/* Count the number of LupIO-blk devices in the system */
	for_each_compatible_node(np, NULL, LUPIO_BLK_OF_COMPATIBLE)
		count++;
	if (!count)
		return -ENODEV;

	/* Create lupio_blk_driver structure */
	lupio_blk_driver = kzalloc(sizeof(*lupio_blk_driver), GFP_KERNEL);
	if (!lupio_blk_driver) {
		pr_err("%s: could not allocate lupio_blk_driver\n", __func__);
		return -ENOMEM;
	}
	lupio_blk_driver->num = count;

	lupio_blk_driver->blks = kzalloc(sizeof(struct lupio_blk_data *) *
			count, GFP_KERNEL);
	if (!lupio_blk_driver->blks) {
		pr_err("%s: could not allocate private data structure\n",
		       __func__);
		err = -ENOMEM;
		goto error_ioc_driver;
	}

	/* Allocate major number for device */
	lupio_blk_driver->major = register_blkdev(0, DEVICE_NAME);
	if (lupio_blk_driver->major <= 0) {
		pr_err("%s: unable to get major number\n", __func__);
		err = lupio_blk_driver->major;
		goto error_alloc_ioc;
	}

	pr_info("%s: LupIO-blk driver, major=%i\n",
		__func__, lupio_blk_driver->major);

	/* Register our driver with platform (device probing will be triggered
	 * from this call) */
	err = platform_driver_register(&lupio_blk_pf_driver);
	if (err) {
		pr_err("%s: could not register platform driver\n", __func__);
		goto error_register_blk;
	}

	return 0;

error_register_blk:
	unregister_blkdev(lupio_blk_driver->major, DEVICE_NAME);
error_alloc_ioc:
	kfree(lupio_blk_driver->blks);
error_ioc_driver:
	kfree(lupio_blk_driver);
	return err;
}

static void __exit lupio_blk_exit(void)
{
	pr_debug("%s: unregistering LupIO-blk driver\n", __func__);

	platform_driver_unregister(&lupio_blk_pf_driver);
	unregister_blkdev(lupio_blk_driver->major, DEVICE_NAME);
	kfree(lupio_blk_driver->blks);
	kfree(lupio_blk_driver);
}

module_init(lupio_blk_init);
module_exit(lupio_blk_exit);

/* MODULE information */
MODULE_AUTHOR("Joël Porquet-Lupine <joel@porquet.org");
MODULE_DESCRIPTION("LupIO-blk driver");
MODULE_LICENSE("GPL");
