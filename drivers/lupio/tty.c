// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2020 Joël Porquet-Lupine <joel@porquet.org>
 */
#include <linux/console.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/serial_core.h>
#include <linux/slab.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/circ_buf.h>

#include <asm/io.h>

#define TX_BUF_SIZE	2048

/*
 * Driver for LupIO-tty
 */

struct lupio_tty_data {
	spinlock_t lock;
	struct device *dev;
	struct tty_port port;
	void __iomem *virt_base;
	unsigned int rx_irq;
	unsigned int index;
	struct console console;

	unsigned char tx_buf[TX_BUF_SIZE];	/* transmit circular buffer */
	unsigned int head;	/* circular buffer head */
	unsigned int tail;	/* circular buffer tail */
};

static struct tty_driver *lupio_tty_driver;

/* LupIO-tty register map */
#define LUPIO_TTY_WRIT		0x0 // Character to print
#define LUPIO_TTY_READ		0x4 // Character to read
#define LUPIO_TTY_CTRL		0x8 // Interrupt enable
#define LUPIO_TTY_STAT		0xC // Device ready

/* Same fields for CTRL and STAT registers */
#define LUPIO_TTY_WBIT (1 << 0)
#define LUPIO_TTY_RBIT (1 << 1)


/*
 * Utils
 */

static char lupio_tty_do_ischar(struct lupio_tty_data *tty)
{
	return !!(readl(tty->virt_base + LUPIO_TTY_STAT) & LUPIO_TTY_RBIT);
}

static char lupio_tty_do_getchar(struct lupio_tty_data *tty)
{
	return readl(tty->virt_base + LUPIO_TTY_READ);
}

static void lupio_tty_do_putchar(struct lupio_tty_data *tty, const char c)
{
	writel(c, tty->virt_base + LUPIO_TTY_WRIT);
}

static void lupio_tty_do_putchar_sync(struct lupio_tty_data *tty, const char c)
{
	while (!(readl(tty->virt_base + LUPIO_TTY_STAT) & LUPIO_TTY_WBIT));
	writel(c, tty->virt_base + LUPIO_TTY_WRIT);
}

static void lupio_tty_do_lower_irq(struct lupio_tty_data *tty)
{
	readl(tty->virt_base + LUPIO_TTY_WRIT);
}

static void lupio_tty_enable_tx_irq(struct lupio_tty_data *tty)
{
	writel(LUPIO_TTY_RBIT | LUPIO_TTY_WBIT, tty->virt_base + LUPIO_TTY_CTRL);
}


static void lupio_tty_disable_tx_irq(struct lupio_tty_data *tty)
{
	writel(LUPIO_TTY_RBIT, tty->virt_base + LUPIO_TTY_CTRL);
}

/*
 * Console driver
 */

static void lupio_tty_console_write(struct console *co, const char *buf, unsigned int count)
{
	struct lupio_tty_data *tty = co->data;

	unsigned long irq_flags;

	spin_lock_irqsave(&tty->lock, irq_flags);

	while (count-- && *buf) {
		if (*buf == '\n')
			lupio_tty_do_putchar_sync(tty, '\r');
		lupio_tty_do_putchar_sync(tty, *buf);
		buf++;
	}

	spin_unlock_irqrestore(&tty->lock, irq_flags);

}

static struct tty_driver *lupio_tty_console_device(struct console *co, int *index)
{
	*index = co->index;
	return lupio_tty_driver;
}

static int lupio_tty_console_setup(struct console *co, char *options)
{
	struct lupio_tty_data *tty = co->data;

	if ((unsigned)co->index >= lupio_tty_driver->num)
		return -ENODEV;
	if (tty->virt_base == 0)
		return -ENODEV;
	return 0;
}

/*
 * TTY driver
 */

/* tty ISR */
static irqreturn_t lupio_tty_interrupt(int irq, void *dev_id)
{
	unsigned long irq_flags;
	struct lupio_tty_data *tty = dev_id;
	unsigned char c;
	unsigned int status;
	
	spin_lock_irqsave(&tty->lock, irq_flags);
	
	status = readl(tty->virt_base + LUPIO_TTY_STAT);

	/* Handle RX IRQ*/
	if (status & LUPIO_TTY_RBIT) {
		c = lupio_tty_do_getchar(tty);
		tty_insert_flip_char(&tty->port, c, TTY_NORMAL);
		tty_flip_buffer_push(&tty->port);
	}

	/* Handle TX IRQ*/
	if (status & LUPIO_TTY_WBIT) {
		lupio_tty_do_lower_irq(tty);
		if (CIRC_CNT(tty->head, tty->tail, TX_BUF_SIZE) > 0) {
			lupio_tty_do_putchar(tty, tty->tx_buf[tty->tail]);
			tty->tail = (tty->tail + 1) & (TX_BUF_SIZE - 1);
		}
		else {
			/* If TX buffer is empty, disable TX IRQ*/
			lupio_tty_disable_tx_irq(tty);
		}
	}

	spin_unlock_irqrestore(&tty->lock, irq_flags);

	return IRQ_HANDLED;
}

/* tty port operations */
static int lupio_tty_port_activate(struct tty_port *port, struct tty_struct *tty_st)
{
	struct lupio_tty_data *tty = tty_st->driver_data;
	int ret;

	/* Associate interrupt to handler */
	ret = devm_request_irq(tty->dev, tty->rx_irq, lupio_tty_interrupt, 0,
			"lupio_tty", tty);
	if (ret) {
		dev_err(tty->dev, "could not request rx irq %u (ret=%i)\n",
				tty->rx_irq, ret);
		return ret;
	}

	/* Enable interrupt at hardware level */
	writel(LUPIO_TTY_RBIT, tty->virt_base + LUPIO_TTY_CTRL);

	return 0;
}

static void lupio_tty_port_shutdown(struct tty_port *port)
{
	struct lupio_tty_data *tty = port->tty->driver_data;

	/* Disable interrupt at hardware level */
	writel(0, tty->virt_base + LUPIO_TTY_CTRL);

	devm_free_irq(tty->dev, tty->rx_irq, tty);
}

static const struct tty_port_operations lupio_tty_port_ops = {
	.activate = lupio_tty_port_activate,
	.shutdown = lupio_tty_port_shutdown,
};

/* tty operations */
static int lupio_tty_install(struct tty_driver *driver, struct tty_struct *tty_st)
{
	struct lupio_tty_data *tty = ((struct lupio_tty_data**)
			driver->driver_state)[tty_st->index];

	tty_st->driver_data = tty;

	return tty_port_install(&tty->port, driver, tty_st);
}

static int lupio_tty_open(struct tty_struct *tty_st, struct file *filp)
{
	struct lupio_tty_data *tty = tty_st->driver_data;

	if (!tty->dev)
		return -ENODEV;

	return tty_port_open(&tty->port, tty_st, filp);
}

static void lupio_tty_close(struct tty_struct *tty_st, struct file *filp)
{
	struct lupio_tty_data *tty = tty_st->driver_data;

	if (tty->dev)
		tty_port_close(&tty->port, tty_st, filp);
}

static void lupio_tty_hangup(struct tty_struct *tty_st)
{
	struct lupio_tty_data *tty = tty_st->driver_data;

	tty_port_hangup(&tty->port);
}

static int lupio_tty_write(struct tty_struct *tty_st, const unsigned char *buf, int count)
{
	struct lupio_tty_data *tty = tty_st->driver_data;
	unsigned long irq_flags;
	unsigned int index;

	index = count;

	spin_lock_irqsave(&tty->lock, irq_flags);

	while (index-- && *buf) {
		if (*buf == '\n') {

			/* if we encounter a newline, add a carriage return before the newline */
			if (CIRC_SPACE(tty->head, tty->tail, TX_BUF_SIZE) > 0) {
				tty->tx_buf[tty->head] = '\r';
				tty->head = (tty->head + 1) & (TX_BUF_SIZE - 1);
			}
		}

		/* Add character to the end of the buffer */
		if (CIRC_SPACE(tty->head, tty->tail, TX_BUF_SIZE) > 0) {
			tty->tx_buf[tty->head] = *buf;
			tty->head = (tty->head + 1) & (TX_BUF_SIZE - 1);
		}

		buf++;
    }

	/* enable TX IRQ and initiate first transaction*/
	if (CIRC_CNT(tty->head, tty->tail, TX_BUF_SIZE) > 0) {
		lupio_tty_enable_tx_irq(tty);

		/* If transmitter is ready, send first character*/
		if ((readl(tty->virt_base + LUPIO_TTY_STAT) & LUPIO_TTY_WBIT)) {
			lupio_tty_do_putchar(tty, tty->tx_buf[tty->tail]);
			tty->tail = (tty->tail + 1) & (TX_BUF_SIZE - 1);
		}
	}

	spin_unlock_irqrestore(&tty->lock, irq_flags);

	return count;
}

static unsigned int lupio_tty_write_room(struct tty_struct *tty_st)
{
	struct lupio_tty_data *tty = tty_st->driver_data;

	unsigned long flags;
	unsigned int count;

	spin_lock_irqsave(&tty->lock, flags);
	count = CIRC_SPACE(tty->head, tty->tail, TX_BUF_SIZE);
	spin_unlock_irqrestore(&tty->lock, flags);

	return count;
}

static unsigned int lupio_tty_chars_in_buffer(struct tty_struct *tty_st)
{
	struct lupio_tty_data *tty = tty_st->driver_data;

	return lupio_tty_do_ischar(tty);
}

static const struct tty_operations lupio_tty_ops = {
	.install = lupio_tty_install,
	.open = lupio_tty_open,
	.close = lupio_tty_close,
	.hangup = lupio_tty_hangup,
	.write = lupio_tty_write,
	.write_room = lupio_tty_write_room,
	.chars_in_buffer = lupio_tty_chars_in_buffer,
};

/*
 * Platform driver
 */

static int lupio_tty_pf_probe(struct platform_device *pdev)
{
	struct lupio_tty_data **lupio_ttys;
	struct lupio_tty_data *tty;
	unsigned int index;
	int ret = -EINVAL;

	/* get our private structure */
	lupio_ttys = lupio_tty_driver->driver_state;

	/* look for a free index */
	for (index = 0; index < lupio_tty_driver->num; index++)
		if (lupio_ttys[index] == NULL)
			break;
	if (index >= lupio_tty_driver->num)
		return -EINVAL;

	/* allocate a new lupio_tty_data instance */
	tty = devm_kzalloc(&pdev->dev, sizeof(struct lupio_tty_data),
			GFP_KERNEL);
	if (!tty) {
		dev_err(&pdev->dev, "failed to allocate memory for %s node\n",
				pdev->name);
		return -ENOMEM;
	}
	lupio_ttys[index] = tty;
	tty->index = index;

	/* Initialize the buffer*/
	tty->head = 0;
	tty->tail = 0;

	/* get the virq of the rx_irq that links the tty to the parent intc.
	 * Note that the virq has already been computed beforehand, with respect
	 * to the irq_domain it belongs to. */
	tty->rx_irq = platform_get_irq(pdev, 0);
	if (tty->rx_irq < 0) {
		dev_err(&pdev->dev, "failed to get virq for %s node\n",
				pdev->name);
		return -EINVAL;
	}

	tty->virt_base = devm_platform_ioremap_resource(pdev, 0);
	if (!tty->virt_base) {
		dev_err(&pdev->dev, "failed to ioremap_resource for %s node\n",
				pdev->name);
		return -EADDRNOTAVAIL;
	}

	/* complete the initialization of our private data structure */
	spin_lock_init(&tty->lock);
	tty_port_init(&tty->port);
	tty->port.ops = &lupio_tty_port_ops;

	tty->dev = tty_port_register_device(&tty->port, lupio_tty_driver,
			index, &pdev->dev);

	if (IS_ERR(tty->dev)) {
		dev_err(&pdev->dev, "could not register lupio_tty (ret=%d)\n",
				ret);
		return PTR_ERR(tty->dev);
	}

	platform_set_drvdata(pdev, tty);

	/* configure console */
	strcpy(tty->console.name, "ttyLIO");
	tty->console.write = lupio_tty_console_write;
	tty->console.device = lupio_tty_console_device;
	tty->console.setup = lupio_tty_console_setup;
	tty->console.flags = CON_PRINTBUFFER;
	tty->console.index = index;
	tty->console.data = tty;
	register_console(&tty->console);

	return 0;
}

static int lupio_tty_pf_remove(struct platform_device *pdev)
{
	struct lupio_tty_data **lupio_ttys = lupio_tty_driver->driver_state;
	struct lupio_tty_data *tty = platform_get_drvdata(pdev);

	unregister_console(&tty->console);
	tty_unregister_device(lupio_tty_driver, tty->index);
	tty_port_destroy(&tty->port);

	lupio_ttys[tty->index] = NULL;

	return 0;
}

#define LUPIO_TTY_OF_COMPATIBLE "lupio,tty"

static const struct of_device_id lupio_tty_pf_of_ids[] = {
	{ .compatible = LUPIO_TTY_OF_COMPATIBLE },
	{}
};
MODULE_DEVICE_TABLE(of, lupio_tty_pf_of_ids);

static struct platform_driver lupio_tty_pf_driver = {
	.driver = {
		.owner 		= THIS_MODULE,
		.name		= "lupio_tty",
		.of_match_table = lupio_tty_pf_of_ids,
	},
	.probe	= lupio_tty_pf_probe,
	.remove = lupio_tty_pf_remove,
};

/*
 * Module initialization and termination
 */

static int __init lupio_tty_init(void)
{
	struct device_node *np;
	unsigned int count = 0;
	int err;
	struct lupio_tty_data **lupio_ttys;

	pr_debug("%s: registering LupIO-tty driver\n", __func__);

	/* count the number of tty channels in the system */
	for_each_compatible_node(np, NULL, LUPIO_TTY_OF_COMPATIBLE)
		count++;

	if (!count)
		return -ENODEV;

	/* create tty_driver structure */
	lupio_tty_driver = tty_alloc_driver(count, TTY_DRIVER_RESET_TERMIOS |
					    TTY_DRIVER_REAL_RAW |
					    TTY_DRIVER_DYNAMIC_DEV);
	if (IS_ERR(lupio_tty_driver)) {
		pr_err("%s: could not allocate tty driver\n", __func__);
		return PTR_ERR(lupio_tty_driver);
	}

	/* create lupio_tty private data structure */
	lupio_ttys = kzalloc(sizeof(struct lupio_tty_data *) * count, GFP_KERNEL);
	if (!lupio_ttys) {
		pr_err("%s: could not allocate private data structure\n",
		       __func__);
		err = -ENOMEM;
		goto error_tty_driver;
	}
	lupio_tty_driver->driver_state = lupio_ttys;

	/* initialize tty_driver structure */
	lupio_tty_driver->driver_name = "lupio_tty";
	lupio_tty_driver->name = "ttyLIO";
	lupio_tty_driver->type = TTY_DRIVER_TYPE_SERIAL;
	lupio_tty_driver->subtype = SERIAL_TYPE_NORMAL;
	lupio_tty_driver->init_termios = tty_std_termios;
	lupio_tty_driver->init_termios.c_oflag &= ~ONLCR;
	tty_set_operations(lupio_tty_driver, &lupio_tty_ops);

	/* register tty_driver structure */
	err = tty_register_driver(lupio_tty_driver);
	if (err) {
		pr_err("%s: could not register tty driver\n",
		       __func__);
		goto error_alloc_tty;
	}

	/* register platform driver
	 * note: device probing will already happen in this call */
	err = platform_driver_register(&lupio_tty_pf_driver);
	if (err) {
		pr_err("%s: could not register platform driver\n",
		       __func__);
		goto error_tty_register;
	}

	return 0;

error_tty_register:
	tty_unregister_driver(lupio_tty_driver);
error_alloc_tty:
	kfree(lupio_tty_driver->driver_state);
error_tty_driver:
	tty_driver_kref_put(lupio_tty_driver);
	return err;
}

static void __exit lupio_tty_exit(void)
{
	pr_debug("%s: unregistering LupIO-tty driver\n", __func__);

	platform_driver_unregister(&lupio_tty_pf_driver);
	tty_unregister_driver(lupio_tty_driver);
	kfree(lupio_tty_driver->driver_state);
	tty_driver_kref_put(lupio_tty_driver);
}

module_init(lupio_tty_init);
module_exit(lupio_tty_exit);

#ifdef CONFIG_LUPIO_TTY_EARLYCON
static void lupio_tty_early_putchar(struct uart_port *port, unsigned char c)
{
	while (!(readl(port->membase + LUPIO_TTY_STAT) & LUPIO_TTY_WBIT));
	writel_relaxed(c, port->membase);
}

static void lupio_tty_early_write(struct console *con, const char *s,
				  unsigned int n)
{
	struct earlycon_device *dev = con->data;

	uart_console_write(&dev->port, s, n, lupio_tty_early_putchar);
}

static int __init lupio_tty_early_setup(struct earlycon_device *dev,
					const char *opt)
{
	if (!dev->port.membase)
		return -ENODEV;

	dev->con->write = lupio_tty_early_write;
	return 0;
}

OF_EARLYCON_DECLARE(lupio_tty, LUPIO_TTY_OF_COMPATIBLE, lupio_tty_early_setup);
#endif /* CONFIG_LUPIO_TTY_EARLYCON */

/* MODULE information */
MODULE_AUTHOR("Joël Porquet-Lupine <joel@porquet.org>");
MODULE_DESCRIPTION("LupIO-tty driver");
MODULE_LICENSE("GPL");
