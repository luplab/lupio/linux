// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2021 Joël Porquet-Lupine <joel@porquet.org>
 */
#include <linux/cpuhotplug.h>
#include <linux/cpumask.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/irqchip.h>
#include <linux/irqchip/chained_irq.h>
#include <linux/irqdesc.h>
#include <linux/irqdomain.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>

#include <asm/io.h>
#ifdef CONFIG_ARM
#include <asm/smp_plat.h>
#else
#include <asm/smp.h>
#endif

/*
 * Driver for LupIO-IPI
 */

struct lupio_ipi_data {
	void __iomem *virt_base;
	struct irq_domain *irq_domain;
	unsigned int parent_irq, base_ipi;
};
static struct lupio_ipi_data *ipi;

/* LupIO-IPI register map */
#define LUPIO_IPI_MASK	0x0	// IRQ mask
#define LUPIO_IPI_PEND	0x4	// IRQ pending

#define LUPIO_IPI_MAX	0x8	// Stride for SMP

#define LUPIO_IPI_REG(idx, reg)	((idx) * LUPIO_IPI_MAX + reg)

#define LUPIO_IPI_NR	32	// 32 possible soft IRQs

/*
 * Interrupt handler
 */
static void lupio_ipi_interrupt_chained(struct irq_desc *desc)
{
	struct irq_chip *chip = irq_desc_get_chip(desc);
	unsigned int cpuid = cpu_logical_map(smp_processor_id());
	unsigned long ipi_pending;
	unsigned int ipinr;


	chained_irq_enter(chip, desc);

	/* Get pending IPIs */
	ipi_pending = readl(ipi->virt_base + LUPIO_IPI_REG(cpuid, LUPIO_IPI_PEND));
	for_each_set_bit(ipinr, &ipi_pending, LUPIO_IPI_NR)
		/* Each IPI will be acknowledged (and lowered) when handled */
		generic_handle_domain_irq(ipi->irq_domain, ipinr);

	chained_irq_exit(chip, desc);
}

/*
 * Irqchip driver
 */
static void __lupio_ipi_set(struct irq_data *d, unsigned int cpuid,
			    unsigned long reg, bool level)
{
	irq_hw_number_t hwirq = irqd_to_hwirq(d);
	u32 mask, irq_mask = 1UL << hwirq;

	mask = readl(ipi->virt_base + LUPIO_IPI_REG(cpuid, reg));
	if (level)
		mask |= irq_mask;
	else
		mask &= ~irq_mask;
	writel(mask, ipi->virt_base + LUPIO_IPI_REG(cpuid, reg));
}

static void lupio_ipi_mask(struct irq_data *d)
{
	unsigned int cpuid = cpu_logical_map(smp_processor_id());
	__lupio_ipi_set(d, cpuid, LUPIO_IPI_MASK, false);
}

static void lupio_ipi_unmask(struct irq_data *d)
{
	unsigned int cpuid = cpu_logical_map(smp_processor_id());
	__lupio_ipi_set(d, cpuid, LUPIO_IPI_MASK, true);
}

static void lupio_ipi_ack(struct irq_data *d)
{
	unsigned int cpuid = cpu_logical_map(smp_processor_id());
	__lupio_ipi_set(d, cpuid, LUPIO_IPI_PEND, false);
}

static void lupio_ipi_send_mask(struct irq_data *d, const struct cpumask *mask)
{
	int cpu;

	for_each_cpu(cpu, mask) {
		__lupio_ipi_set(d, cpu_logical_map(cpu), LUPIO_IPI_PEND, true);
	}
}

static struct irq_chip lupio_ipi_chip = {
	.name			= "lupio-ipi",
	.irq_ack		= lupio_ipi_ack,
	.irq_mask		= lupio_ipi_mask,
	.irq_unmask		= lupio_ipi_unmask,
	.ipi_send_mask		= lupio_ipi_send_mask,
};

/*
 * IRQ domain
 */
static int lupio_ipi_irq_domain_alloc(struct irq_domain *d,
				      unsigned int virq,
				      unsigned int nr_irqs, void *args)
{
	int i;

	for (i = 0; i < nr_irqs; i++) {
		irq_set_percpu_devid(virq + i);
		irq_domain_set_info(d, virq + i, i, &lupio_ipi_chip,
				    d->host_data,
				    handle_percpu_devid_irq,
				    NULL, NULL);
	}

	return 0;
}

static void lupio_ipi_irq_domain_free(struct irq_domain *d,
				      unsigned int virq,
				      unsigned int nr_irqs)
{
	/* Not freeing IPIs */
}

static const struct irq_domain_ops lupio_ipi_irq_domain_ops = {
	.alloc	= lupio_ipi_irq_domain_alloc,
	.free	= lupio_ipi_irq_domain_free,
};

/*
 * CPU hotplug
 */
static int lupio_ipi_cpu_init(unsigned int cpu)
{
	enable_percpu_irq(ipi->parent_irq, IRQ_TYPE_NONE);
#ifdef CONFIG_RISCV
	/* Enable single IPI */
	enable_percpu_irq(ipi->base_ipi, 0);
#endif
	return 0;
}

static int lupio_ipi_cpu_deinit(unsigned int cpu)
{
	disable_percpu_irq(ipi->parent_irq);
#ifdef CONFIG_RISCV
	/* Enable single IPI */
	disable_percpu_irq(ipi->base_ipi);
#endif
	return 0;
}

/*
 * Arch wrapper functions
 */
#ifdef CONFIG_RISCV
static void lupio_riscv_ipi_send(const struct cpumask *mask)
{
	int cpu;

	for_each_cpu(cpu, mask) {
		unsigned int cpuid = cpu_logical_map(cpu);
		writel(1, ipi->virt_base + LUPIO_IPI_REG(cpuid, LUPIO_IPI_PEND));
	}
}

static void lupio_riscv_ipi_clear(void)
{
	unsigned int cpuid = cpu_logical_map(smp_processor_id());

	writel(0, ipi->virt_base + LUPIO_IPI_REG(cpuid, LUPIO_IPI_PEND));
}

static struct riscv_ipi_ops lupio_riscv_ipi_ops = {
	.ipi_inject = lupio_riscv_ipi_send,
	.ipi_clear = lupio_riscv_ipi_clear,
};
#endif

/*
 * OF/Device tree driver
 */
int __init lupio_ipi_of_init(struct device_node *of_node,
			     struct device_node *parent)
{
	unsigned int cpuid = cpu_logical_map(smp_processor_id());
	int err;

	ipi = kzalloc(sizeof(*ipi), GFP_KERNEL);
	if (!ipi) {
		err = -ENOMEM;
		goto error;
	}

	/* Map IPI in memory */
	ipi->virt_base = of_io_request_and_map(of_node, 0, of_node->name);
	if (IS_ERR(ipi->virt_base)) {
		err = PTR_ERR(ipi->virt_base);
		goto error_free;
	}

	/* Create IRQ domain for the IPI (and its bits per word) */
	ipi->irq_domain = irq_domain_add_linear(of_node, LUPIO_IPI_NR,
						&lupio_ipi_irq_domain_ops,
						NULL);
	if (!ipi->irq_domain) {
		pr_err("%s: failed to add IRQ domain\n", __func__);
		err = -ENOMEM;
		goto error_map;
	}

	irq_domain_update_bus_token(ipi->irq_domain, DOMAIN_BUS_IPI);
	ipi->base_ipi = irq_domain_alloc_irqs(ipi->irq_domain, LUPIO_IPI_NR,
					      NUMA_NO_NODE, NULL);
	if (ipi->base_ipi < 0) {
		pr_err("%s: failed to allocate IRQs\n", __func__);
		err = ipi->base_ipi;
		goto error_domain;
	}

	/* Configure IRQ for all the IPI outputs */
	ipi->parent_irq = irq_of_parse_and_map(of_node, cpuid);
	if (!ipi->parent_irq) {
		pr_err("Failed to map IRQ\n");
		err = -EINVAL;
		goto error_domain;
	}
	irq_set_chained_handler(ipi->parent_irq, lupio_ipi_interrupt_chained);

	/* Install hotplug callbacks to configure IPI */
	err = cpuhp_setup_state(CPUHP_AP_LUPIO_IPI_STARTING,
				"irqchip/lupio/ipi:starting",
				lupio_ipi_cpu_init,
				lupio_ipi_cpu_deinit);
	if (err)
		goto error_irq;

	/* Register our callbacks to arch code */
#ifdef CONFIG_RISCV
	/* Register just one IPI */
	err = request_percpu_irq(ipi->base_ipi, NULL, "IPI", &irq_stat);
	if (err)
		goto error_state;

	/* With RISCV, the IRQs from an IPI is processed by the CPU's intc
	 * directly. Hence, our lupio_ipi_interrupt_chained will never be
	 * called. Instead, the RV intc driver calls our
	 * lupio_riscv_ipi_ops.lupio_ipi_clear to acknowledge the IPI. */
	riscv_set_ipi_ops(&lupio_riscv_ipi_ops);

#elif defined(CONFIG_ARM) || defined(CONFIG_MIPS)
	set_smp_ipi_range(ipi->base_ipi, LUPIO_IPI_NR);
#endif

	return 0;

#ifdef CONFIG_RISCV
error_state:
	cpuhp_remove_state(CPUHP_AP_LUPIO_IPI_STARTING);
#endif
error_irq:
	irq_dispose_mapping(ipi->parent_irq);
error_domain:
	irq_domain_remove(ipi->irq_domain);
error_map:
	iounmap(ipi->virt_base);
error_free:
	kfree(ipi);
error:
	pr_err("Failed to initialize node %s (err=%d)\n", of_node->name, err);
	return err;
}

IRQCHIP_DECLARE(lupio_ipi, "lupio,ipi", lupio_ipi_of_init);
