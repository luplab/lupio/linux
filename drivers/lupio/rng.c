// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2020 Joël Porquet-Lupine <joel@porquet.org>
 */
#include <linux/circ_buf.h>
#include <linux/completion.h>
#include <linux/hw_random.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/random.h>

#include <asm/io.h>

/*
 * Driver for LupIO-RNG
 */

#define RNG_BUFFER_SIZE 64

struct lupio_rng_data {
	void __iomem *virt_base;
	struct hwrng rng;
	int irq;
	spinlock_t lock;

	u32 buffer[RNG_BUFFER_SIZE];
	unsigned int head;
	unsigned int tail;

	bool req_active;
	struct completion rng_done;
};

/*
 * RNG driver
 */

/* Register map */
#define LUPIO_RNG_SEED		0x4
#define LUPIO_RNG_CTRL		0x8 // interrupt enable / request new random number
#define LUPIO_RNG_STAT		0xC // Device ready
#define LUPIO_RNG_BUSY		(1 << 0)

/* CTRL register field */
#define LUPIO_RNG_IRQE		0x1

static void lupio_rng_do_lower_irq(struct lupio_rng_data *lupio_rng)
{
	readl(lupio_rng->virt_base + LUPIO_RNG_STAT);
}

static int lupio_rng_init(struct hwrng *rng)
{
	u32 seed;

	struct lupio_rng_data *lupio_rng = (void*)rng->priv;

	/* Initialize lupio-rng with a random seed */
	get_random_bytes(&seed, sizeof(seed));
	writel(seed, lupio_rng->virt_base + LUPIO_RNG_SEED);

	return 0;
}

static int lupio_rng_read(struct hwrng *rng, void *buf, size_t max, bool wait)
{
	struct lupio_rng_data *lupio_rng = (void*)rng->priv;
	u32 *data = buf;
	size_t words = max / sizeof(u32);
	size_t count = 0;
	size_t first_chunk = 0;
	unsigned long irq_flags;
	int ret;
	int available;

	spin_lock_irqsave(&lupio_rng->lock, irq_flags);
	available = CIRC_CNT(lupio_rng->head, lupio_rng->tail, RNG_BUFFER_SIZE);

	count = min((size_t)available, words);

	/* Since the data may wrap around in the circular buffer, copy in two chunks */
	if (count > 0) {
		first_chunk = CIRC_CNT_TO_END(lupio_rng->head, lupio_rng->tail, RNG_BUFFER_SIZE);
		memcpy(data, &lupio_rng->buffer[lupio_rng->tail], first_chunk * sizeof(u32));

		if (count > first_chunk)
			memcpy(data + first_chunk, lupio_rng->buffer, (count - first_chunk) * sizeof(u32));

		lupio_rng->tail = (lupio_rng->tail + count) & (RNG_BUFFER_SIZE - 1);
	}

	/* If the buffer is less than half full, start a request to start refilling it */
	if (!(lupio_rng->req_active) && (CIRC_SPACE(lupio_rng->head, lupio_rng->tail, RNG_BUFFER_SIZE) >= RNG_BUFFER_SIZE / 2)) {
		writel(LUPIO_RNG_IRQE, lupio_rng->virt_base + LUPIO_RNG_CTRL);
		lupio_rng->req_active = 1;
	}

	spin_unlock_irqrestore(&lupio_rng->lock, irq_flags);

	if (!wait || count == words)
		goto done;

	/* If we still need more numbers, add then to the buffer one by one */
	while (count < words) {

			/* Wait for new numbers to be generated */
			ret = wait_for_completion_killable(&lupio_rng->rng_done);

			if (ret < 0) {
				return ret;
			}

		spin_lock_irqsave(&lupio_rng->lock, irq_flags);

		/* Consume from the circular buffer */
		data[count] = lupio_rng->buffer[lupio_rng->tail];
		lupio_rng->tail = (lupio_rng->tail + 1) & (RNG_BUFFER_SIZE - 1);
		count++;

		spin_unlock_irqrestore(&lupio_rng->lock, irq_flags);
	}

	done:
		return count * sizeof(*data);
}

static irqreturn_t lupio_rng_interrupt(int irq, void *dev_id)
{
	struct lupio_rng_data *lupio_rng = dev_id;
	unsigned long irq_flags;

	spin_lock_irqsave(&lupio_rng->lock, irq_flags);

	/* Store random number in the buffer */
	lupio_rng->buffer[lupio_rng->head] = __raw_readl(lupio_rng->virt_base);
	lupio_rng->head = (lupio_rng->head + 1) & (RNG_BUFFER_SIZE - 1);

	/* Acknowledge the interrupt */
	lupio_rng_do_lower_irq(lupio_rng);

	/* If we have space in the circular buffer, request a new number */
	if (CIRC_SPACE(lupio_rng->head, lupio_rng->tail, RNG_BUFFER_SIZE)) {
		writel(LUPIO_RNG_IRQE, lupio_rng->virt_base + LUPIO_RNG_CTRL);
		lupio_rng->req_active = 1;
	} else {
		lupio_rng->req_active = 0;
	}

	spin_unlock_irqrestore(&lupio_rng->lock, irq_flags);

	/* Wake up a waiting reader */
	complete(&lupio_rng->rng_done);

	return IRQ_HANDLED;
}

/*
 * Platform driver
 */
static int lupio_rng_pf_probe(struct platform_device *pdev)
{
	struct lupio_rng_data *lupio_rng;
	int ret;

	lupio_rng = devm_kzalloc(&pdev->dev, sizeof(struct lupio_rng_data),
				 GFP_KERNEL);
	if (!lupio_rng) {
		dev_err(&pdev->dev, "failed to allocate memory for %s node\n",
			pdev->name);
		return -ENOMEM;
	}

	lupio_rng->virt_base = devm_platform_ioremap_resource(pdev, 0);
	if (!lupio_rng->virt_base) {
		dev_err(&pdev->dev, "failed to ioremap_resource for %s node\n",
			pdev->name);
		return -EADDRNOTAVAIL;
	}

	spin_lock_init(&lupio_rng->lock);
	init_completion(&lupio_rng->rng_done);

	/* Initialize circular buffer pointers */ 
	lupio_rng->head = 0;
	lupio_rng->tail = 0;

	lupio_rng->req_active = 0;

	/* Get the IRQ number from the device tree */
	lupio_rng->irq = platform_get_irq(pdev, 0);
	if (lupio_rng->irq < 0) {
		dev_err(&pdev->dev, "failed to get virq for %s node\n",
				pdev->name);
		return -EINVAL;
	}

	/* Associate interrupt to handler */
	ret = devm_request_irq(&pdev->dev, lupio_rng->irq, lupio_rng_interrupt, 0,
			"lupio_rng", lupio_rng);

	if (ret) {
		dev_err(&pdev->dev, "could not request irq %u (ret=%i)\n",
				lupio_rng->irq, ret);
		return ret;
	}

	lupio_rng->rng.name = pdev->name;
	lupio_rng->rng.read = lupio_rng_read;
	lupio_rng->rng.priv = (unsigned long)lupio_rng;
	lupio_rng->rng.init = lupio_rng_init;

	platform_set_drvdata(pdev, lupio_rng);

	return devm_hwrng_register(&pdev->dev, &lupio_rng->rng);
}

static const struct of_device_id lupio_rng_pf_of_ids[] = {
	{ .compatible = "lupio,rng" },
	{}
};
MODULE_DEVICE_TABLE(of, lupio_rng_pf_of_ids);

static struct platform_driver lupio_rng_pf_driver = {
	.driver = {
		.owner 		= THIS_MODULE,
		.name		= "lupio_rng",
		.of_match_table = lupio_rng_pf_of_ids,
	},
	.probe	= lupio_rng_pf_probe,
};

module_platform_driver(lupio_rng_pf_driver);

/* MODULE information */
MODULE_AUTHOR("Joël Porquet-Lupine <joel@porquet.org>");
MODULE_DESCRIPTION("LupIO-RNG driver");
MODULE_LICENSE("GPL");
