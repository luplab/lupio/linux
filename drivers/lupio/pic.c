// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2021 Joël Porquet-Lupine <joel@porquet.org>
 */
#include <linux/cpumask.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/irqchip.h>
#include <linux/irqchip/chained_irq.h>
#include <linux/irqdesc.h>
#include <linux/irqdomain.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/spinlock.h>

#include <asm/io.h>
#ifdef CONFIG_ARM
#include <asm/exception.h>
#include <asm/smp_plat.h>
#else
#include <asm/smp.h>
#endif

/*
 * Driver for LupIO-PIC
 */
#define LUPIO_PIC "lupio-pic"

#define LUPIO_PIC_NSRC	32	// 32 supported sources
#define LUPIO_PIC_NCPU	32	// 32 cpus max

#if defined(CONFIG_SMP) && CONFIG_LUPIO_PIC_PERCPU > 0
#define LUPIO_PIC_PCPU		CONFIG_LUPIO_PIC_PERCPU // Percpu IRQs
#define LUPIO_PIC_PCPU_MAX	(LUPIO_PIC_PCPU * CONFIG_NR_CPUS)
#endif


struct lupio_pic_data {
	void __iomem *virt_base;
	struct irq_domain *irq_domain;
	unsigned int irq_to_cpuid[LUPIO_PIC_NSRC];
};
static struct lupio_pic_data *pic;


/* LupIO-PIC register map */
#define LUPIO_PIC_PRIO		0x0	// Highest-priority active IRQ
#define LUPIO_PIC_MASK		0x4	// IRQ mask
#define LUPIO_PIC_PEND		0x8	// IRQ pending
#define LUPIO_PIC_ENAB		0xC	// IRQ enable

#define LUPIO_PIC_MAX		0x10	// Stride for SMP

#define LUPIO_PIC_REG(idx, reg)	((idx) * LUPIO_PIC_MAX + reg)

/*
 * Interrupt handler
 */
static inline void lupio_pic_interrupt(void)
{
	unsigned int cpuid = cpu_logical_map(smp_processor_id());
	irq_hw_number_t hwirq;

	/* Get highest-priority IRQ */
	hwirq = readl(pic->virt_base + LUPIO_PIC_REG(cpuid, LUPIO_PIC_PRIO));

	/* Only associated cpu should receive IRQ */
	BUG_ON(cpuid != pic->irq_to_cpuid[hwirq]);

#if defined(CONFIG_SMP) && CONFIG_LUPIO_PIC_PERCPU > 0
	if (hwirq < LUPIO_PIC_PCPU_MAX) {
		BUG_ON(hwirq < LUPIO_PIC_PCPU * cpuid
		       && hwirq >= LUPIO_PIC_PCPU * cpuid + LUPIO_PIC_PCPU);
		/* Transform to percpu IRQ in range [0:LUPIO_PIC_PCPU) */
		hwirq = hwirq % LUPIO_PIC_PCPU;
	}
#endif

	generic_handle_domain_irq(pic->irq_domain, hwirq);
}

/* If LupIO-PIC is the platform interrupt controller (e.g., ARM32) */
static void
#if defined(CONFIG_ARM)
__exception_irq_entry
#endif
lupio_pic_interrupt_arch(struct pt_regs *regs)
{
	lupio_pic_interrupt();
}

/* If LupIO-PIC is chained to a parent interrupt controller (e.g., RISC-V) */
static void lupio_pic_interrupt_chained(struct irq_desc *desc)
{
	struct irq_chip *chip = irq_desc_get_chip(desc);

	chained_irq_enter(chip, desc);

	lupio_pic_interrupt();

	chained_irq_exit(chip, desc);
}

/*
 * Irqchip driver
 */
static void __lupio_pic_set(struct irq_data *d, unsigned long reg, bool level)
{
	irq_hw_number_t hwirq = irqd_to_hwirq(d);
	unsigned int cpuid;
	u32 mask, irq_mask;

#if defined(CONFIG_SMP) && CONFIG_LUPIO_PIC_PERCPU > 0
	/* IRQ has to be in range [0:LUPIO_PIC_PCPU) if percpu IRQ or it has to
	 * be a shared IRQ */
	BUG_ON(hwirq >= LUPIO_PIC_PCPU && hwirq < LUPIO_PIC_PCPU_MAX);

	if (hwirq < LUPIO_PIC_PCPU) {
		/* Transform to actual IRQ input for percpu IRQ */
		hwirq = cpu_logical_map(smp_processor_id()) * LUPIO_PIC_PCPU
			+ hwirq;
	}
#endif

	cpuid = pic->irq_to_cpuid[hwirq];
	irq_mask = 1UL << hwirq;

	mask = readl(pic->virt_base + LUPIO_PIC_REG(cpuid, reg));
	if (level)
		mask |= irq_mask;
	else
		mask &= ~irq_mask;
	writel(mask, pic->virt_base + LUPIO_PIC_REG(cpuid, reg));
}

static void lupio_pic_mask(struct irq_data *d)
{
	__lupio_pic_set(d, LUPIO_PIC_MASK, false);
}

static void lupio_pic_unmask(struct irq_data *d)
{
	__lupio_pic_set(d, LUPIO_PIC_MASK, true);
}

static void lupio_pic_enable(struct irq_data *d)
{
	__lupio_pic_set(d, LUPIO_PIC_ENAB, true);
	lupio_pic_unmask(d);
}

static void lupio_pic_disable(struct irq_data *d)
{
	lupio_pic_mask(d);
	__lupio_pic_set(d, LUPIO_PIC_ENAB, false);
}

#ifdef CONFIG_SMP
static DEFINE_RAW_SPINLOCK(pic_lock);

static int lupio_pic_set_affinity(struct irq_data *d,
				  const struct cpumask *dest, bool force)
{
	irq_hw_number_t hwirq = irqd_to_hwirq(d);
	unsigned int new_cpu;

#if CONFIG_LUPIO_PIC_PERCPU > 0
	/* Percpu IRQs can never migrate */
	BUG_ON(hwirq < LUPIO_PIC_PCPU_MAX);
#endif

	/* Find CPU to receive IRQ */
	if (force)
		new_cpu = cpumask_first(dest);
	else
		new_cpu = cpumask_first_and(dest, cpu_online_mask);
	if (new_cpu >= nr_cpu_ids)
		return -EINVAL;

	/* Migrate IRQ from old cpu to new cpu */
	raw_spin_lock(&pic_lock);
	lupio_pic_disable(d);
	pic->irq_to_cpuid[hwirq] = cpu_logical_map(new_cpu);
	lupio_pic_enable(d);
	raw_spin_unlock(&pic_lock);

	irq_data_update_effective_affinity(d, cpumask_of(new_cpu));

	return IRQ_SET_MASK_OK;
}

#endif
static struct irq_chip lupio_pic_chip = {
	.name			= "lupio-pic",
	.irq_enable		= lupio_pic_enable,
	.irq_disable		= lupio_pic_disable,
	.irq_mask		= lupio_pic_mask,
	.irq_unmask		= lupio_pic_unmask,
#ifdef CONFIG_SMP
	.irq_set_affinity	= lupio_pic_set_affinity,
#endif
};

/*
 * IRQ domain
 */
static int lupio_pic_irq_domain_map(struct irq_domain *d, unsigned int virq,
				    irq_hw_number_t hw)
{
#if defined(CONFIG_SMP) && CONFIG_LUPIO_PIC_PERCPU > 0
	unsigned int cpuid = cpu_logical_map(smp_processor_id());

	/* Percpu IRQ */
	if (hw < LUPIO_PIC_PCPU_MAX) {
		/* Check that actual IRQ number belongs to current CPU */
		BUG_ON(hw < LUPIO_PIC_PCPU * cpuid
		       && hw >= LUPIO_PIC_PCPU * cpuid + LUPIO_PIC_PCPU);
		/* Transform to percpu IRQ in range [0:LUPIO_PIC_PCPU) */
		hw = hw % LUPIO_PIC_PCPU;

		irq_set_percpu_devid(virq);
		irq_set_chip_and_handler(virq, &lupio_pic_chip,
					 handle_percpu_devid_irq);

		return 0;
	}
#endif
	/* Shared IRQ */
	irq_set_chip_and_handler(virq, &lupio_pic_chip,
				 handle_level_irq);
	irq_set_status_flags(virq, IRQ_LEVEL | IRQ_NOPROBE);

	return 0;
}

static const struct irq_domain_ops lupio_pic_irq_domain_ops = {
	.map = lupio_pic_irq_domain_map,
	.xlate = irq_domain_xlate_onecell,
};

/*
 * OF/Device tree driver
 */
int __init lupio_pic_of_init(struct device_node *of_node,
			     struct device_node *parent)
{
	unsigned int irq;
	unsigned int cpuid = cpu_logical_map(smp_processor_id());
	irq_hw_number_t hwirq;
	int err = 0;

	pic = kzalloc(sizeof(*pic), GFP_KERNEL);
	if (!pic) {
		err = -ENOMEM;
		goto error;
	}

	/* Create IRQ domain for the PIC (and its input IRQs) */
	pic->irq_domain = irq_domain_add_linear(of_node, LUPIO_PIC_NSRC,
						&lupio_pic_irq_domain_ops,
						NULL);
	if (!pic->irq_domain) {
		pr_err(LUPIO_PIC ": Failed to add IRQ domain\n");
		err = -ENOMEM;
		goto error_free;
	}

	/* Map PIC in memory */
	pic->virt_base = of_io_request_and_map(of_node, 0, of_node->name);
	if (IS_ERR(pic->virt_base)) {
		pr_err(LUPIO_PIC ": Failed to map PIC\n");
		err = PTR_ERR(pic->virt_base);
		goto error_domain;
	}

	/* Association between IRQs and CPUs */
	hwirq = 0;

#if defined(CONFIG_SMP)
	/* Remove default association between all IRQs and CPU #0 */
	writel(0UL, pic->virt_base + LUPIO_PIC_REG(0, LUPIO_PIC_ENAB));

#if CONFIG_LUPIO_PIC_PERCPU > 0
	/* Associate percpu IRQs to proper CPU if any */
	for (; hwirq < LUPIO_PIC_PCPU_MAX; hwirq++) {
		pic->irq_to_cpuid[hwirq] = hwirq / LUPIO_PIC_PCPU;
	}
#endif
#endif

	/* Associate all shared IRQ to boot CPU by default */
	for (; hwirq < LUPIO_PIC_NSRC; hwirq++) {
		pic->irq_to_cpuid[hwirq] = cpuid;
	}

	/* Configure IRQ for all the PIC's outputs */
	irq = irq_of_parse_and_map(of_node, cpuid);
	if (irq <= 0) {
		/* On ARM cpus for instance, the CPU doesn't have its own intc
		 * so this driver is the platform driver */
		irq_set_default_host(pic->irq_domain);
		set_handle_irq(lupio_pic_interrupt_arch);
	} else {
		/* Attach our IRQ handler */
		irq_set_chained_handler(irq, lupio_pic_interrupt_chained);
	}

	return 0;

error_domain:
	irq_domain_remove(pic->irq_domain);
error_free:
	kfree(pic);
error:
	pr_err(LUPIO_PIC ": Failed to initialize node %s (err=%d)\n",
	       of_node->name, err);
	return err;
}

IRQCHIP_DECLARE(lupio_pic, "lupio,pic", lupio_pic_of_init);
